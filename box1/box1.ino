//#include <DFRobotDFPlayerMini.h>

#include <Adafruit_NeoPixel.h>

#include <DFRobotDFPlayerMini.h>

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

#define PIN            12
#define NUMPIXELS      8
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
 
SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

unsigned long time;
int R = 0,G = 0,B = 0;//LED RGB
byte LED_speed = 10;

int iBatterieStatus[] = {0,310,320,340,360,380}; //Batterie Status

int iButtonActive = 1;
int iButtonColor[][3] = { {0,0,0}, {0,5,25},{25,0,25},{25,25,25},{25,0,0},{25,19,0},{0,25,0},  {0,5,25},{25,0,25},{25,25,25},{25,0,0},{25,19,0},{0,25,0} };

//Kalibrierung Lautstärke Min-Max
//200K + 10K
//int iVolueToleranceMin[] = { 0,33,38,43,48,53,61, 90,150,175,270};
//int iVolueToleranceMax[] = {32,36,41,46,51,59,85,140,165,250,1000};
//500K + 0
int iVolueToleranceMin[] = { 0,65,95,125,155,185,215,245,275,305,335};
int iVolueToleranceMax[] = {50,80,110,140,170,200,230,260,290,320,1000};

int iValue = 0;//Momentan aktive Lautstärke
int iValueFaktor = 3; //Die Lautstärke kann von 0-30 übergeben werden, jedoch bei 10 Stuffen kann der Facktor vergeben werden. Kann als Kinder Sicherung genutzt werden. Z.B. 10*1=10| 10*2=20| 10*3=30

int iBatteryVolts; //Batterie Volt

byte row[]={2,3,4,5};
//int col[]={6,7,8,9};
byte col[]={6,7,8,9};
//byte col_scan;
//byte i,j; 
bool BUSY = false;//fase = music dont play

byte activeButton = 1;
byte playFolder = 1; //1-36
byte GroupFolder = 0; //Funktionstasten 1,2,3 (0,12,24)

void setup()
{
  pixels.begin();
  mySoftwareSerial.begin(9600);
  Serial.begin(19200);
  
  for(byte i=0;i<sizeof(col)/sizeof(byte);i++)//Button
  {
    pinMode(row[i],OUTPUT);
  }
  for(byte j=0; j<sizeof(col)/sizeof(byte); j++)
  {
    pinMode(col[j],INPUT);
    digitalWrite(col[j],HIGH);
  }

  pinMode(6,INPUT);//DFlayer - BUSY
  
  
  Serial.println("Start");
  Serial.print("BatterieStatus: ");
  Serial.println(getBatteryStatus());
  if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
   // while(true);
  }
 
  myDFPlayer.setTimeOut(500); 
 // myDFPlayer.playFolder(1, 1);
  
  //myDFPlayer.play(1);  //Play the first mp3
  //myDFPlayer.enableLoop();
  //myDFPlayer.EQ(DFPLAYER_EQ_NORMAL);
  myDFPlayer.EQ(DFPLAYER_EQ_CLASSIC);
  
  myDFPlayer.loopFolder(playFolder);
  myDFPlayer.volume(0);  //Set volume value. From 0 to 30

  //myDFPlayer.sendStack(0x46);
 // waitAvailable
  //read();
}


int isPause = 0;
void loop()
{
  time = millis();
  //BUSY = (digitalRead(6) == LOW);

iBatteryVolts = getBatteryVolts();//Setze aktuellen Stand der Batterie-Volt

byte btn = keypress(0);

if(btn > 0 && btn < 13)
{
  Serial.print("Button Press: ");
  Serial.print(btn);
  Serial.print(" GroupButton: ");
  Serial.print(GroupFolder);
  Serial.println();
  
  if(playFolder != GroupFolder+btn)
  {
    Serial.print(" Open New Folder: ");
    Serial.print(GroupFolder+btn);
    Serial.println();
    myDFPlayer.loopFolder(GroupFolder+btn);
    delay(500);
    BUSY = (digitalRead(6) == LOW);//ToDo: Funktioniert nicht!!! Soll ermitteln ob gerade abgespeitl wird oder nicht
    if(!BUSY && !isPause)
    {
      Serial.println("Button hat keine Musik");
    }
    playFolder = GroupFolder+btn;
    activeButton = btn;
    
    //LED_round(iButtonColor[btn][0]*iValue,iButtonColor[btn][1]*iValue,iButtonColor[btn][2]*iValue);//LED
    R = iButtonColor[btn][0]*iValue;
    G = iButtonColor[btn][1]*iValue;
    B = iButtonColor[btn][2]*iValue;
  }
  else
  {
    Serial.print(" Play Next in Folder: ");
    Serial.print(GroupFolder+btn);
    Serial.println();
    myDFPlayer.next();
  }
  //delay(1000);
  //Serial.print("PLAY File:");
  //Serial.println(myDFPlayer.readCurrentFileNumber());
}
else if(btn > 12 && btn < 17) //Sonder Tasten Top-Button
{
  switch(btn){
    case 13:
      GroupFolder = 0;
      break;
    case 14:
      GroupFolder = 12;
      break;
    case 15:
      GroupFolder = 24;
      break;
    }
  Serial.print("GroupButton: ");
  Serial.print(GroupFolder);
  Serial.println();
}

  //Lautstärke
  int iVolume = ControlVolume();
  
  if(iValue != iVolume && iVolume != -1)
  {
    iValue = iVolume;
    Serial.print("Lautstaerke Stuffe: ");
    Serial.print(iValue);
    Serial.println();
   
    //LED_round(iButtonColor[playFolder][0]*iValue,iButtonColor[playFolder][1]*iValue,iButtonColor[playFolder][2]*iValue);//LED
    R = iButtonColor[activeButton][0]*iValue;
    G = iButtonColor[activeButton][1]*iValue;
    B = iButtonColor[activeButton][2]*iValue;
    LED_speed = iValue;
    
    if(iValue == 0) //Pause wenn zu leise
    {
      if(isPause == 0)
      {
        Serial.print("PAUSE");Serial.println();
        isPause = 1;
        myDFPlayer.pause();
      }
    }
    else
    {
      if(isPause == 1)
      {
         Serial.print("PLAY");Serial.println();
         myDFPlayer.start();
         isPause = 0;
      }
      myDFPlayer.volume(iValue*iValueFaktor);
    }
  }

  LED_round(R,G,B);
}

/**
 * return 0 = no press; 1-16 für Button
*/
byte keypress(byte iLevel)
{
  //http://www.circuitstoday.com/interfacing-hex-keypad-to-arduino
  byte iReturn = 0;
  for(byte i=0; i<sizeof(row)/sizeof(byte);i++)
  {
    for(byte i2=0; i2<sizeof(row)/sizeof(byte);i2++)
    {
      digitalWrite(row[i2],HIGH);
    }
    digitalWrite(row[i],LOW);
    for(byte j=0; j<sizeof(col)/sizeof(byte); j++)
    {
      byte col_scan=digitalRead(col[j]);
      if(col_scan==LOW)
      {
        Serial.print(i);Serial.print('x');Serial.println(j); 
        if(i==1&&j==0)
          iReturn = 1;
        else if(i==1&&j==1)
          iReturn = 2;
        else if(i==1&&j==2)
          iReturn = 3;
        else if(i==1&&j==3)
          iReturn = 4;
        else if(i==2&&j==0)
          iReturn = 5;
        else if(i==2&&j==1)
          iReturn = 6;
        else if(i==2&&j==2)
          iReturn = 7;
        else if(i==2&&j==3)
          iReturn = 8;
        else if(i==3&&j==0)
          iReturn = 9;
        else if(i==3&&j==1)
          iReturn = 10;
        else if(i==3&&j==2)
          iReturn = 11;
        else if(i==3&&j==3)
          iReturn = 12;
        else if(i==0&&j==0)
          iReturn = 13;
        else if(i==0&&j==1)
          iReturn = 14;
        else if(i==0&&j==2)
          iReturn = 15;
        else if(i==0&&j==3) //Dieser exsistiert nicht
          iReturn = 16;
      }
    }
  }

  if(iReturn > 0)
  {
    delay(300);
    iReturn = iReturn + (iLevel*100);
  }
  return iReturn;
}

int ControlVolume()
{
  //Serial.println(analogRead(0));
  int iReturn = -1;//-1
  int sensorValue1 = map(analogRead(0), 0, 1023, 0, iBatteryVolts );//berechne anhand Batterie stärke - analogRead(A0);
  if(sensorValue1)
  {
    //Serial.print(" Lautstaerke Sensor: ");
    //Serial.println(sensorValue1);
    byte iAnz = sizeof(iVolueToleranceMin)/sizeof(int);
    
    for(byte i=0; i < iAnz;i++)
    {
      if (sensorValue1 > iVolueToleranceMin[i] && sensorValue1 < iVolueToleranceMax[i])
      {
        iReturn = i;
      }
    }
  }
  return iReturn;
}


void LED(int iR,int iG, int iB)
{
   Serial.print(" Color: R: ");
   Serial.print(iR);
   Serial.print(" G: ");
   Serial.print(iR);
   Serial.print(" B: ");
   Serial.print(iB);
   Serial.println();
  for(int i=0;i<NUMPIXELS;i++)
  {
      pixels.setPixelColor(i, pixels.Color(iR,iG,iB));
      pixels.show();
     
       // pixels.clear();
  }
}

//Effect
byte nextPixel = 0;
unsigned long nextTime = 0;
void LED_round(int iR, int iG, int iB)
{
  if(time > nextTime+1000/LED_speed)
  {
    pixels.clear();
    pixels.setPixelColor(nextPixel, pixels.Color(iR/2,iG/2,iB/2));
    nextPixel++;
    if(nextPixel > NUMPIXELS-1)
      nextPixel = 0;
    pixels.setPixelColor(nextPixel, pixels.Color(iR,iG,iB));
    pixels.show();
    nextTime = time;
  }
}


int getBatteryStatus() //0-5
{
    iBatteryVolts=getBatteryVolts();  //Determins what actual Vcc is, (X 100), based on known bandgap voltage
    Serial.print("Battery Vcc volts =  ");
    Serial.print(iBatteryVolts);
    Serial.println();
   
    int iAnz = sizeof(iBatterieStatus)/sizeof(int);
    for(int i=iAnz-1;i>0;i--)
    {
      if(iBatteryVolts > iBatterieStatus[i])
        return i;
    }
}

int getBatteryVolts(void) // Returns actual value of Vcc (x 100)
{
        
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
     // For mega boards
     const long InternalReferenceVoltage = 1115L;  // Adjust this value to your boards specific internal BG voltage x1000
        // REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc reference
        // MUX4 MUX3 MUX2 MUX1 MUX0  --> 11110 1.1V (VBG)         -Selects channel 30, bandgap voltage, to measure
     ADMUX = (0<<REFS1) | (1<<REFS0) | (0<<ADLAR)| (0<<MUX5) | (1<<MUX4) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (0<<MUX0);
  
#else
     // For 168/328 boards
     const long InternalReferenceVoltage = 1056L;  // Adjust this value to your boards specific internal BG voltage x1000
        // REFS1 REFS0          --> 0 1, AVcc internal ref. -Selects AVcc external reference
        // MUX3 MUX2 MUX1 MUX0  --> 1110 1.1V (VBG)         -Selects channel 14, bandgap voltage, to measure
     ADMUX = (0<<REFS1) | (1<<REFS0) | (0<<ADLAR) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (0<<MUX0);
       
#endif
     delay(50);  // Let mux settle a little to get a more stable A/D conversion
        // Start a conversion  
     ADCSRA |= _BV( ADSC );
        // Wait for it to complete
     while( ( (ADCSRA & (1<<ADSC)) != 0 ) );
        // Scale the value
     int results = (((InternalReferenceVoltage * 1024L) / ADC) + 5L) / 10L; // calculates for straight line value 
     return results;
 
}
