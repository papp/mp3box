// Required libraries:

#include <SPI.h>
//#include <SD.h>
#include <SdFat.h>
//#include <SdFatUtil.h>
#include <SFEMP3Shield.h>
#include <PinChangeInt.h>

#include "PCF8575.h" //Buttons

//RGB Ring

/* 
#include <Adafruit_NeoPixel.h>

#define PIN        5
#define NUMPIXELS 8
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
*/

//Button
PCF8575 PCF[3] = {PCF8575(0x20),PCF8575(0x21),PCF8575(0x22)};

//SD Card
SdFat sd;
SdFile rootX;
SdFile fileX;

//MP3 Player
SFEMP3Shield MP3player;


 // Turn off amplifier chip / turn on MP3 mode:
 #define SHDN_GPIO1 A2
 

unsigned long myTimer;
unsigned long myTimer2;
//======Button Class Start ==============
class CButton {

  public:
  byte Id; //Button Nummer 0-12
  byte bRGB[3] = {0,0,0};
  const char* Dir; //Pfad
  
  void begin(byte id) {
    Id = id;
  }
   
 void setLed(bool r, bool g, bool b) {
  //Button/Farben/ControlerID:Port
  bRGB[0] = r;
  bRGB[1] = g;
  bRGB[2] = b;
  byte bCode[13][3][2] = {
    {{1,11},{1,12},{1,13}}, //Button 0
    {{1, 8},{1, 9},{1,10}}, //Button 1
    {{1, 3},{1, 4},{1,5}},
    {{1, 0},{1, 1},{1, 2}},
    {{0, 3},{0, 4},{0, 5}},
    {{0, 0},{0, 1},{0, 2}},
    {{2,13},{2,14},{2,15}},
    {{2,10},{2,11},{2,12}},
    {{2, 7},{2, 8},{2, 9}},
    {{2, 4},{2, 5},{2, 6}},
    {{2, 1},{2, 2},{2, 3}},
    {{1,14},{1,15},{2, 0}},
    {{0, 6},{1, 6},{1, 7}}
  };
  
  PCF[ bCode[Id][0][0] ].write(bCode[Id][0][1],r);
  PCF[ bCode[Id][1][0] ].write(bCode[Id][1][1],g);
  PCF[ bCode[Id][2][0] ].write(bCode[Id][2][1],b);
 }

  bool isPressed() {
    bool bReturn = 0;
 
    byte bCode[16][2] = {
     {14, 9},{14, 8},{13,11},{13,10},
     {13, 9},{13, 8},{15,11},{15,10},
     {15, 9},{15, 8},{14,11},{14,10},
     {12, 8},{12, 9},{12,10},{12,11}
    };
    
    PCF[0].write(bCode[Id][0],0);
    if(PCF[0].readButton(bCode[Id][1]) == 0 && (millis() - myTimer) > 500) {
      myTimer = millis();
      Serial.print(F("Button: ") );
      Serial.println( Id );
      bReturn = 1;
      byte rgb[3] = {bRGB[0],bRGB[1],bRGB[2]};//Stellt die ursprüngliche Farbe beim nächsten durchlauf wieder her
      setLed(1,0,0);
      bRGB[0] = rgb[0];
      bRGB[1] = rgb[1];
      bRGB[2] = rgb[2];
    }
    else {
      setLed(bRGB[0],bRGB[1],bRGB[2]);
    }
    PCF[0].write(bCode[Id][0],1);
    
    return bReturn;
  }



  /* ToDo:
  void interupt Funktion dieser ständig beim durchlauf aufgeruffen wird. Kann so Timer Funtkion erfüllen.
  */
};
CButton Button[13];
//======Button Class End =================
//char dir[] = "/Geschichten/Bibi/";

byte iCurrentGroup = 0; //Ordner Gruppe (0-9)
byte iCurrentButton = 0; // aktiver Button (0-12)
byte bCurrentVolume = 0; //0-100
bool bIsPause = 0;

//char CurrentTrack[255] = "";
void setup()
{
  byte result;

  Serial.begin(9600);
  Serial.println(F("MP3 Player Start"));
  //Serial.print("Free RAM = "); Serial.println(FreeRam(), DEC);

  //Button LED
  for(byte i=0; i<3; i++) {
    PCF[i].begin();
    PCF[i].write16(LOW);
  }

  //Button
  for(byte i=0; i < 13; i++) {
    Button[i].begin(i);
  }
    
    //SD Karte Vorhanden und kann gelesen werden 1=grün oder 1=rot = fehler
    result = sd.begin(SD_SEL, SPI_HALF_SPEED);
    /*
    if (result != 1)
      Button[0].setLed(1,0,0);
    else 
      Button[0].setLed(0,1,0);
      */
      
    //MP3 Player vorhanden
    result = MP3player.begin();
    /*
    if((result != 0) && (result != 6))
      Button[1].setLed(1,0,0);
    else 
      Button[1].setLed(0,1,0);
      */

  switchGroup(0);
  //rootX.open(getDirFile(iCurrentButton + iCurrentGroup*100));
  Button[12].setLed(0,1,0);//Menubutton
  
  //MP3 Player Start
  //MP3player.begin();
  MP3player.setVolume(80,80);



/*      
    //SD einlesen ToDo: Muss ordner öffnen und MP3 darin zählen können
    if (!root.open("/")) {
     Serial.println("Fehler beim Öffnen der SD");
    }
Serial.print("Anzahl: ");
 Serial.println( getDir("/") );

    char csvName[100];
    while (file.openNext(&root, O_RDONLY)) {
      if (!file.isHidden()) {
      }
      
      if (file.isFile()) {
        Serial.print("ist Datei: ");
        file.getName(csvName,sizeof(csvName));
        Serial.println(csvName);
       }
       if (file.isDir()) {
        Serial.print("ist Ordner: ");
        file.getName(csvName,sizeof(csvName));
        Serial.println(csvName);

        if(!root.open(csvName)) {
          while (file.openNext(&root, O_RDONLY)) {
            if (file.isFile()) {
            Serial.print("ist Datei: ");
            file.getName(csvName,sizeof(csvName));
            Serial.println(csvName);
            }
          }
        }
        
       }
      file.close();
    }
*/
    
    //sd.ls(,LS_R);
  // Turn on amplifier chip
  pinMode(SHDN_GPIO1, OUTPUT);
  digitalWrite(SHDN_GPIO1, LOW);
  digitalWrite(SHDN_GPIO1, HIGH);
  delay(2);


 /// CurrentFolder = "/00/";
 /// rootX.open(CurrentFolder);
 // rootX.open("/");

 //checkAllButton2Folder("/");

 //RGB Ring
  //pixels.begin(); 
  //pixels.show();            // Turn OFF all pixels ASAP
  //pixels.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)
  //pixels.setPixelColor(0, pixels.Color(0, 0, 100));
    
} // END setup





//######LOOP====================================================
void loop()
{
 
    //Volumen
    int8_t vol = ControlVolume(); //gibt auktuellen Stand vom Volumen Regler
    if(vol > 5) {
      //short val_1 = map(vol,0,100, 100,0); //Muss wert drehen
      byte val_1 = map(vol,0,100, 100,0); //Muss wert drehen
      MP3player.setVolume(val_1,val_1);
      MP3player.resumeMusic(); // play 
      bIsPause = 0;
      Button[iCurrentButton].setLed(0,0,1); // blau
    
    } else {
      MP3player.pauseMusic(); // Pause
      bIsPause = 1;
      Button[iCurrentButton].setLed(1,0,0); // rot
     
    }


    
    //changeVolume(vol);
 /* 
   if(MP3player.isPlaying() && iCurrentVolume == 0) {
     stopPlaying();
   }
   else if(!MP3player.isPlaying() && iCurrentVolume > 0 ) {
     startPlaying();
   }
 */

/*
for(byte i = 0; i< 13;i++) {
  //Button[i].setLed(0,0,0);
  
  if(Button[i].isPressed()) {
    //Button[i].setLed(1,0,0);
    Serial.print( " Open: '" ); Serial.print( Button[i].Dir );Serial.println( "'" );
    loadDir(Button[i].Dir,i);

  }
}
*/

  //Press Button 0-12
  for(byte i = 0; i <13;i++) {
    if(Button[i].isPressed()) {

        if( i<12) { // nur Playbare Buttons 0-11
          if(MP3player.isPlaying()) {
            MP3player.pauseMusic(); // Muss vorher gestoppt werden
          }
          if( sd.exists(getDir( i + iCurrentGroup*100)) ) { // Wenn ordner nicht exsistiert, dann nicht umschalten!
            if(iCurrentButton != i) { //Nur beim Button Wechsel den alten Button wieder zurück setzen
              Button[iCurrentButton].setLed(0,1,0);
            }
            iCurrentButton = i;
            playNext(iCurrentButton + iCurrentGroup*100);
           /* const char* DirName = ButtonDir(i);
            playNext(DirName);*/
          }
        }
        else if(i == 12) { // Menu Button
          iCurrentButton = 0; // Setze Play Ordner auf den ersten
          
          if(!switchGroup(iCurrentGroup+1) && iCurrentGroup < 9) { // Wenn das nicht erfolgreich war, dann Gruppe zurück auf 0 setzen
            switchGroup(0); // Setze Gruppe zurück ans anfang
          }
          //===================
          playNext(iCurrentButton + iCurrentGroup*100);
  
        }
      
      MP3player.resumeMusic();
      
    }
  }

 if(iCurrentButton < 12) {
   if(!MP3player.isPlaying()) { // spielt nicht, dann fang an!
    //const char* DirName = ButtonDir(iCurrentButton);
     playNext(iCurrentButton + iCurrentGroup*100);
   }
   else {
    Button[iCurrentButton].setLed(0,0,1);
   }
 }





  /*
  //Button Auslesen
  byte bButton = bbutton(); //Button Check
  if (bButton > 0) {
    Serial.print("Button: "); Serial.println(bButton);
    if(bButton == 1) {
      
//       startPlaying();
       LEDButton(bButton,0,1,0);
    }
  }
  **/
  
  

//openFolder("aa");
//openFolder("bbb"); 
}

/*
const char* ButtonDir (byte BtnID) {
  
  static char buf[6];
  snprintf(buf, sizeof(buf), "/%03u/", BtnID );
  Serial.print( F("ButtonDir: "));Serial.println(buf);
  return buf;
}
*/

//ToDo: Funktioniert nicht, Probleme beim root Open/ close  Anzahl Abspielbare Dateien im Verzeichnis. -1 = Verzeicnis exsistiert nicht 
int8_t iDirCountOrEmpty(uint16_t iDir) {
 uint8_t iReturn = -1;
 char* Dir = getDir(iDir);
  //snprintf(Dir, sizeof(Dir), "/%03u/", iDir );
  
  if(sd.exists(Dir)) {
    Serial.print( F("Dir Exsistiert JA: "));Serial.println(Dir);
    iReturn = 0;
    char Name[50];
    /*
    rootX.open(Dir);
    while (fileX.openNext(&rootX, O_RDONLY)) {
      fileX.getName(Name,sizeof(Name));
      if (fileX.isFile() && !fileX.isSubDir() && !fileX.isHidden() && Name[0] != '.') {
          char DirName[5] = {"\0"};
          //char* DirName;
          strcpy(DirName,Dir);
          strcat(DirName,Name);
          if(isPlayable(DirName)) {
              iReturn++;
          }
      }
      fileX.close(); // Schließe Datei
      delay(2);
    }
    rootX.close();
    */
    iReturn = 1;
  }
  else {
    iReturn = -1;// Verzeichnis exsistiert nicht
  }
  
  //rootX.close();
  //rootX.open(Dir);
  //delay(50);
  return iReturn;
}

char* getDir(uint16_t iDir) {
  static char Dir[6];
  snprintf(Dir, sizeof(Dir), "/%03u/", iDir );
  Serial.print( F("ButtonDir: "));Serial.println(Dir);
  return Dir;
}

//Schaltet Gruppe um, falls nechste Gruppe nicht exsistiert dann false
bool switchGroup(byte iGroup) {
  Serial.print( F("Switch to Group: "));Serial.println(iGroup);
  char * cDir;
  if(MP3player.isPlaying()) {
    MP3player.stopTrack(); // Muss vorher gestoppt werden, weil sonnst kann nicht immer root richtig reopen werden
  }
  for(byte i=0; i < 12; i++) {
    cDir = getDir( i + iGroup*100);
    if( sd.exists(cDir) ) {
      iCurrentGroup = iGroup;
      break;
    }
  }
  
  if(iCurrentGroup == iGroup) { // Gruppe Exsistiert
    //LED-Buttons Blinken
    for(byte j=0; j < iCurrentGroup+1; j++) { // Blinke so oft wie Lavel gewählt wurde
      for(byte i=0; i < 12; i++) {
        Button[i].setLed(0,0,0);
      }
      delay(100);
      for(byte i=0; i < 12; i++) {
        Button[i].setLed(0,1,0);
      }
      delay(100);
    }
    for(byte i=0; i < 12; i++) {
        Button[i].setLed(0,0,0);
    }

   for(byte i=0; i < 12; i++) {
      if( sd.exists(getDir( i + iCurrentGroup*100)) ) {
        Button[i].setLed(0,1,0);
        delay(50);
      }
    }
  }
  return (iCurrentGroup == iGroup);
}


//iDir = 0-12
uint16_t iCurrentDir = 0;
//=000 bis 911
void playNext(uint16_t iDir) {

  //Ordner Selektieren
  //static char Dir[6];
  //snprintf(Dir, sizeof(Dir), "/%03u/", iDir );
  char* Dir = getDir(iDir);
  Serial.print( F("ButtonDir: "));Serial.println(Dir);
  
  //const char* Dir = ButtonDir(iDir);
  char Name[50];
  //uint8_t dirIndex = 0;
  bool bIsEnd = 1;

  if(iCurrentDir != iDir) { // Bei Ordner Wechsel
    if(MP3player.isPlaying()) {
        MP3player.stopTrack(); // Muss vorher gestoppt werden, weil sonnst kann nicht immer root richtig reopen werden
    }
    rootX.close();
    rootX.open(Dir);
    iCurrentDir = iDir;
  }
  
  //SdFile fileX;
  //char * Name;
    MP3player.pauseMusic(); // setze Pause
    Serial.println( F("playNext() "));
    while (fileX.openNext(&rootX, O_RDONLY)) {
      Serial.println( F("openNext() "));
        fileX.getName(Name,sizeof(Name));
        Serial.print( fileX.dirIndex() );Serial.print(F(":"));
        if (fileX.isFile() && !fileX.isSubDir() && !fileX.isHidden() && Name[0] != '.') {
            
            //char* DirName = Dir;// = conCat2((char*)Dir,(char*)&Name);
            char DirName[5] = {"\0"};
            //char* DirName;
            strcpy(DirName,Dir);
            strcat(DirName,Name);
            Serial.print( DirName );
            
            if(isPlayable(DirName)){
              fileX.close(); // Schließe Datei
              if(MP3player.isPlaying()) {
                MP3player.stopTrack();//MP3player.resumeMusic();
              }
             
              MP3player.playMP3(DirName,0);
              Serial.println(F(": Play"));
              bIsEnd = 0;
              break;//soll nur nächste MP3 abspielen
            }
        }
        
        fileX.close(); // Schließe Datei
    }
    
    if(bIsEnd) { //begine Verzeichnis von vorne ToDo: Dass muss beim Verzeichniswechsel durchgeführt werden!!!
      Serial.println(F("reset Ordner"));
      //fileX.close();
      if(MP3player.isPlaying()) {
        MP3player.stopTrack(); // Muss vorher gestoppt werden, weil sonnst kann nicht immer root richtig reopen werden
      }
      rootX.close();Serial.println(F("rootX.close()"));
      rootX.open(Dir);Serial.println(F("rootX.open()"));
      delay(50);
    }
    if(!bIsPause) {
      MP3player.resumeMusic(); // play 
    }
  
}


//Überprüft ob für jeden Button ein Ordner vorhanden ist, ja = grün; nein = schwarz
bool checkAllButton2Folder(const char* Dir) {
  char Name[2];
  for(byte i=0;i<12;i++) { // Deaktiviere alle Buttons
    Button[i].setLed(0,0,0);
  }
  
  rootX.open(Dir);
  while (fileX.openNext(&rootX, O_RDONLY)) {
        if (fileX.isDir()) {
          fileX.getName(Name,sizeof(Name));
          Serial.print(F("CheckButton: "));Serial.println(Name);
            if(Name == "03") { //aktiviere nur die Buttons zur diesen die Zuordnung verfügbar ist.
               Button[3].setLed(0,1,0);
            }
        }
        fileX.close(); // Schließe Datei
    }
  rootX.close(); //schließe Ordner
}



byte getDirCount(const char* Root) {
  byte bReturn = 0;
  SdFile root1;
  SdFile file1;
  char Name[100];
  if (!root1.open(Root)) {
     Serial.println(F("Fehler beim Öffnen der SD"));
   }
 
   while (file1.openNext(&root1, O_RDONLY)) {
    Serial.print("root1: ");
          Serial.println(root1);
      if (file1.isDir()) {
        file1.getName(Name,sizeof(Name));
        if(Name[0] != '.') {
          bReturn++;
          Serial.print(F("ist Ordner: "));
          Serial.println(Name);
        }
      }
      /*
      if (file1.isFile()) {
          Serial.print("ist Datei: ");
          file1.getName(Name,sizeof(Name));
          Serial.println(Name);
       }*/
      file1.close();
   }
   
  return bReturn;
}




const char* conCat(const char* Aa, const char* Bb) {
  return conCat(Aa,Bb,"","");
}
const char* conCat(const char* Aa, const char* Bb, const char* Cc) {
  char* New = new char [ strlen(Aa) + strlen(Bb) + strlen(Cc) ];
  strcpy(New,Aa);
  strcat(New,Bb);
  strcat(New,Cc);
  return New;
}
const char* conCat(const char* Aa, const char* Bb, const char* Cc, const char* Dd) {
  //CurrentFolder = Folder;
  char* New = new char [ strlen(Aa) + strlen(Bb) + strlen(Cc) + strlen(Dd) ];
  strcpy(New,Aa);
  strcat(New,Bb);
  strcat(New,Cc);
  strcat(New,Dd);
  return New;
}

char* conCat2(char* Aa, char* Bb) {
  //CurrentFolder = Folder;
  char* New = new char [ strlen(Aa) + strlen(Bb)  ];
  strcpy(New,Aa);
  strcat(New,Bb);
  return New;
}


//Prüft ob die Datei Abspielbar ist
boolean isPlayable(const char* track)
{
  char *extension;
  
  extension = strrchr(track,'.');
  extension++;
  if (
    (strcasecmp(extension,"MP3") == 0) 
   // || (strcasecmp(extension,"WAV") == 0) ||
   // (strcasecmp(extension,"MID") == 0) ||
   // (strcasecmp(extension,"MP4") == 0) ||
   // (strcasecmp(extension,"WMA") == 0) ||
   // (strcasecmp(extension,"FLA") == 0) ||
   // (strcasecmp(extension,"OGG") == 0) ||
   // (strcasecmp(extension,"AAC") == 0)
  )
    return true;
  else
    return false;
}



short val_1 = -1;
int8_t val_2 = -1;
int8_t ControlVolume()
{
  short v = analogRead(3);
  byte v2 = map(v,20,1010, 0,100);
 
  if(val_1 != v && val_1 != v+5 && val_1 != v-5 && val_2 != v2)
  {
      val_1 = v;
      val_2 = v2;
      
      Serial.print(F(" analogRead: "));Serial.println(v);
  }
  return val_2;
}


/*
void changeVolume(int8_t v) {
  if(iCurrentVolume != v) {
    int8_t v1 = map(v,0,100, 100,0);
    MP3player.setVolume(v1, v1);
    iCurrentVolume = v;
    Serial.print("Valume: ");Serial.print(iCurrentVolume);
    Serial.println("");
  }
}
*/

/*
void getNextTrack()
{
  // Get the next playable track (check extension to be
  // sure it's an audio file)
  
  do
    getNextFile();
  while(isPlayable() != true);
}


void getPrevTrack()
{
  // Get the previous playable track (check extension to be
  // sure it's an audio file)

  do
    getPrevFile();
  while(isPlayable() != true);
}


void getNextFile()
{
  // Get the next file (which may be playable or not)

  int result = (file.openNext(sd.vwd(), O_READ));

  // If we're at the end of the directory,
  // loop around to the beginning:
  
  if (!result)
  {
    sd.chdir("/",true);
    getNextTrack();
    return;
  }
  file.getFilename(track);  
  file.close();
}


void getPrevFile()
{
  // Get the previous file (which may be playable or not)
  
  char test[13], prev[13];

  // Getting the previous file is tricky, since you can
  // only go forward when reading directories.

  // To handle this, we'll save the name of the current
  // file, then keep reading all the files until we loop
  // back around to where we are. While doing this we're
  // saving the last file we looked at, so when we get
  // back to the current file, we'll return the previous
  // one.
  
  // Yeah, it's a pain.

  strcpy(test,track);

  do
  {
    strcpy(prev,track);
    getNextTrack();
  }
  while(strcasecmp(track,test) != 0);
  
  strcpy(track,prev);
}


void startPlaying()
{
  int result;
  
  if (debugging)
  {
    Serial.print(F("playing "));
    Serial.print(track);
    Serial.print(F("..."));
  }  

  result = MP3player.playMP3(track);

  if (debugging)
  {
    Serial.print(F(" result "));
    Serial.println(result);  
  }
}


void stopPlaying()
{
  if (debugging) Serial.println(F("stopping playback"));
  MP3player.stopTrack();
}


boolean isPlayable()
{
  // Check to see if a filename has a "playable" extension.
  // This is to keep the VS1053 from locking up if it is sent
  // unplayable data.

  char *extension;
  
  extension = strrchr(track,'.');
  extension++;
  if (
    (strcasecmp(extension,"MP3") == 0) ||
    (strcasecmp(extension,"WAV") == 0) ||
    (strcasecmp(extension,"MID") == 0) ||
    (strcasecmp(extension,"MP4") == 0) ||
    (strcasecmp(extension,"WMA") == 0) ||
    (strcasecmp(extension,"FLA") == 0) ||
    (strcasecmp(extension,"OGG") == 0) ||
    (strcasecmp(extension,"AAC") == 0)
  )
    return true;
  else
    return false;
}
*/
